<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked wc_print_notices - 10
 */
do_action( 'woocommerce_before_single_product' );

if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.
	return;
}
?>


<?php if(get_field('banner') || get_field('banner_2') || get_field('video_file')):
$show_description_on_right_side = 0;
else:
	$show_description_on_right_side = 1;
endif;
?>

<div id="product-<?php the_ID(); ?>" <?php wc_product_class( '', $product ); ?>>

	<div class="row mx-0">
		<div class="col-md-6 order-md-1 order-2">

	<?php
	/**
	 * Hook: woocommerce_before_single_product_summary.
	 *
	 * @hooked woocommerce_show_product_sale_flash - 10
	 * @hooked woocommerce_show_product_images - 20
	 */
	do_action( 'woocommerce_before_single_product_summary' );
	?>
			
</div>


<div class="col-md-6 d-flex  order-md-2 order-1">
		<div  id="" class="">
				<div class="summary entry-summary">
					<?php
					/**
					 * Hook: woocommerce_single_product_summary.
					 *
					 * @hooked woocommerce_template_single_title - 5
					 * @hooked woocommerce_template_single_rating - 10
					 * @hooked woocommerce_template_single_price - 10
					 * @hooked woocommerce_template_single_excerpt - 20
					 * @hooked woocommerce_template_single_add_to_cart - 30
					 * @hooked woocommerce_template_single_meta - 40
					 * @hooked woocommerce_template_single_sharing - 50
					 * @hooked WC_Structured_Data::generate_product_data() - 60
					 */
					do_action( 'woocommerce_single_product_summary' );


					?>



					<!-- banner video banner 2 -->
					<div class="col-md-12 mb-3 px-0">

					<?php if($show_description_on_right_side==1):?>
						<?php if(ICL_LANGUAGE_CODE == 'en'):?>
							<h3>Description</h3>
							<?php elseif(ICL_LANGUAGE_CODE == 'fr'):?>
								<h3>Description</h3>
							<?php endif;?>
							<div class="content">
								<?php the_content();?>
							</div>
					<?php endif;?>
					

					<?php 
						$image = get_field('banner');
						$size = 'full'; // (thumbnail, medium, large, full or custom size)
						if( $image ):?>
							<?php echo wp_get_attachment_image($image,$size, '',array('class'=>'img-fluid') ); ?>
						<?php endif;
						?>
				
					 <!-- video -->
					<?php if(get_field('video_file')):?>
						<div class="video position-relative my-3">
						<video playsinline autoplay muted loop id="video" style="width: 100%;">
							<source src="<?php echo get_field('video_file'); ?>" type="video/mp4">
						</video>
						<span type="button" class="video-muted"></span>
						</div>
					<?php endif;?>

					<!-- banner 2 -->
					<?php 
					$image = get_field('banner_2');
					$size = 'full'; // (thumbnail, medium, large, full or custom size)
					if( $image ):?>
						<?php echo wp_get_attachment_image($image,$size, '',array('class'=>'img-fluid') ); ?>
					<?php endif;
					?>
			
					</div>
				</div>
		</div>

</div>

	<?php
	/**
	 * Hook: woocommerce_after_single_product_summary.
	 *
	 * @hooked woocommerce_output_product_data_tabs - 10
	 * @hooked woocommerce_upsell_display - 15
	 * @hooked woocommerce_output_related_products - 20
	 */
	do_action( 'woocommerce_after_single_product_summary' );

	?>
	<?php if($show_description_on_right_side==0):?>
	<div class="col-md-10 mx-auto order-3">
		<?php if(ICL_LANGUAGE_CODE == 'en'):?>
			<h3>Description</h3>
			<?php elseif(ICL_LANGUAGE_CODE == 'fr'):?>
				<h3>Description</h3>
			<?php endif;?>
			<div class="content">
				<?php the_content();?>
			</div>
	</div>
	<?php endif;?>
</div>


</div>



<?php do_action( 'woocommerce_after_single_product' ); ?>
