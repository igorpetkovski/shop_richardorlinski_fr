<?php
/**
 * Single Product stock.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/stock.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

?>
<?php if($product->get_stock_quantity() <= 0):?>
 	<div class="col-md-12 bg-primary text-center py-3 mb-3">
		<h3 class="text-white text-uppercase my-0" style="font-family: 'Bitmap Orlinski',sans-serif;"> <?php echo wp_kses_post( $availability ); ?> !</h3>
	</div>
<?php endif;?>
