<?php
/**
 * Customer completed order email
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/customer-completed-order.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates/Emails
 * @version 3.7.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/*
 * @hooked WC_Emails::email_header() Output the email header
 */
do_action( 'woocommerce_email_header', $email_heading, $email ); ?>

<?php /* translators: %s: Customer first name */ ?>
<p><?php printf( esc_html__( 'Hi %s,', 'woocommerce' ), esc_html( $order->get_billing_first_name() ) ); ?></p>
<?php /* translators: %s: Site title */ ?>
<p>
<?php if(get_post_meta($order->get_id(), 'wpml_language', true)=='en'):?>	

<p><?php _e('Your order has just been shipped and should arrive soon!','orlinski'); ?></p>
<p><?php _e('Track your package to learn more about the status of your order.','orlinski'); ?></p>
<p><?php _e('Colissimo tracking number: ','orlinski'); ?>
	<strong>
		<?php switch (get_field('tracking_post',$order->get_id())) {
			case 'DPD':
				$link='https://tracking.dpd.de/status/en_US/parcel/'.get_field('tracking_code',$order->get_id());
				break;
			case 'Swiss Post':
				$link='https://service.post.ch/ekp-web/ui/entry/search/'.get_field('tracking_code',$order->get_id());
				break;		
			case 'Poste Italiane':
				$link='https://www.poste.it/cerca/index.html#/risultati-cerca-nel-sito/'.get_field('tracking_code',$order->get_id());
				break;	
			case 'Poste Italiane':
				$link='https://www.deutschepost.de/sendung/simpleQueryResult.html?form.sendungsnummer='.get_field('tracking_code',$order->get_id());
				break;		
			case 'Bpost':
				$link='https://track.bpost.cloud/btr/web/#/search?lang=en&itemCode='.get_field('tracking_code',$order->get_id()).'&postalCode='.$order->get_shipping_postcode();
				break;											
			default:
				$link = 'https://www.laposte.fr/outils/track-a-parcel?code='.get_field('tracking_code',$order->get_id());
				break;
		}?>
		
	<?php if(get_field('tracking_code',$order->get_id())):
		the_field('tracking_code',$order->get_id());
		endif;?>
		</strong>
	</p>
<a style="display:inline-block; background-color:#ff0000 !important; padding:10px 20px; color:#ffffff; text-transform:uppercase; text-align:center; font-weight:bold;" href="<?php echo $link; ?>">Track Now!</a>
<p><small>Please note, if your tracking number appears invalid, please wait a little longer, the link will be active as soon as La Poste has activated it.</small></p>
<br>	
<p><?php _e('Please find attached the invoice corresponding to your order.','orlinski'); ?></p>

<?php elseif(get_post_meta($order->get_id(), 'wpml_language', true)=='fr'):?>	

<p><?php _e('Votre commande vient d’être expédiée et ne devrait plus tarder à arriver !','orlinski'); ?></p>
<p><?php _e('Suivez votre colis pour en savoir plus sur le statut de votre commande. ','orlinski'); ?></p>
<p><?php _e('Numéro de suivi Colissimo :','orlinski'); ?>
	<strong>
		<?php switch (get_field('tracking_post',$order->get_id())) {
			case 'DPD':
				$link='https://tracking.dpd.de/status/en_US/parcel/'.get_field('tracking_code',$order->get_id());
				break;
			case 'Swiss Post':
				$link='https://service.post.ch/ekp-web/ui/entry/search/'.get_field('tracking_code',$order->get_id());
				break;		
			case 'Poste Italiane':
				$link='https://www.poste.it/cerca/index.html#/risultati-cerca-nel-sito/'.get_field('tracking_code',$order->get_id());
				break;	
			case 'Poste Italiane':
				$link='https://www.deutschepost.de/sendung/simpleQueryResult.html?form.sendungsnummer='.get_field('tracking_code',$order->get_id());
				break;		
			case 'Bpost':
				$link='https://track.bpost.cloud/btr/web/#/search?lang=en&itemCode='.get_field('tracking_code',$order->get_id()).'&postalCode='.$order->get_shipping_postcode();
				break;											
			default:
				$link = 'https://www.laposte.fr/outils/track-a-parcel?code='.get_field('tracking_code',$order->get_id());
				break;
		}?>
		
	<?php if(get_field('tracking_code',$order->get_id())):
		the_field('tracking_code',$order->get_id());
		endif;?>
		</strong>
	</p>
<a style="display:inline-block; background-color:#ff0000 !important; padding:10px 20px; color:#ffffff; text-transform:uppercase; text-align:center; font-weight:bold;" href="<?php echo $link; ?>">Suivre Maintenant!</a>
<p><small>Attention, si votre numéro de suivi apparaît comme invalide, veuillez patienter encore un peu, le lien sera actif dès que La Poste aura procédé à son activation.</small></p>
<br>		
<p><?php _e('Veuillez trouver en pièce jointe la facture correspondant à votre commande.','orlinski'); ?></p>	
<?php endif; ?>

<?php
/*
 * @hooked WC_Emails::order_details() Shows the order details table.
 * @hooked WC_Structured_Data::generate_order_data() Generates structured data.
 * @hooked WC_Structured_Data::output_structured_data() Outputs structured data.
 * @since 2.5.0
 */
do_action( 'woocommerce_email_order_details', $order, $sent_to_admin, $plain_text, $email );

/*
 * @hooked WC_Emails::order_meta() Shows order meta data.
 */
do_action( 'woocommerce_email_order_meta', $order, $sent_to_admin, $plain_text, $email );

/*
 * @hooked WC_Emails::customer_details() Shows customer details
 * @hooked WC_Emails::email_address() Shows email address
 */
do_action( 'woocommerce_email_customer_details', $order, $sent_to_admin, $plain_text, $email );

/**
 * Show user-defined additional content - this is set in each email's settings.
 */
if ( $additional_content ) {
	echo wp_kses_post( wpautop( wptexturize( $additional_content ) ) );
}

/*
 * @hooked WC_Emails::email_footer() Output the email footer
 */
do_action( 'woocommerce_email_footer', $email );
