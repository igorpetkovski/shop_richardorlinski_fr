<?php
/**
 * The template for displaying search forms
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;
?>

<form method="get" id="searchform-big" action="<?php echo esc_url( home_url( '/' ) ); ?>" role="search" class="w-100 bg-white">
	<label class="sr-only" for="s"><?php esc_html_e( 'Search', 'understrap' ); ?></label>
	<div class="input-group">
		<input class="field form-control search-big-input" id="s" name="s" type="text"
			placeholder="<?php if(ICL_LANGUAGE_CODE =='en'): ?>Search...<?php elseif(ICL_LANGUAGE_CODE =='fr'):?>Chercher...<?php endif; ?>" value="<?php the_search_query(); ?>">
			 <input type="hidden" name="post_type" value="products" />
		<span class="input-group-append">
			<input class="btn-search" id="searchsubmit" name="submit" type="submit"
			value="<?php esc_attr_e( 'Search here...', 'understrap' ); ?>">
		</span>
	</div>
</form>
