<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

?>

<?php get_template_part( 'sidebar-templates/sidebar', 'footerfull' ); ?>


	<!-- section 5 -->
	<div class="row mx-0 border-top">
		<div class="col-md-12 my-5 px-md-2 px-0">
			<div class="col-md-5 mx-auto text-center" id="insta">
				<a href="https://instagram.com/orlinskishop/" target="_blank" class="text-dark"><h2 class="my-0"><img class="pb-1" src="<?php echo get_template_directory_uri();?>/img/icon-instagram.svg" width="35px"> @RICHARDORLINSKI</h2></a>
			</div>
		</div>

	</div>
	<!-- section 5 end -->		

	<div class="row mx-0">
		<?php echo do_shortcode('[instagram-feed]');?>
		<?php echo do_shortcode('[instagram-feed num=8 cols=4 showfollow=false]');?>
	</div>
<div class="wrapper border-top" id="wrapper-footer">

	<div class="container big-container my-md-5 my-3">

		<div class="row">
			 
			      <div class="col-md-2 col-6 my-md-2 my-4">
			       <?php dynamic_sidebar( 'footer-1' ); ?>

			      </div>
			      	<div class="col-md-2 col-12  my-md-2 my-4">
				  		  <?php if(ICL_LANGUAGE_CODE =='en'): ?>
					  <p class="text-muted mt-0 mb-1">Our customer service is available by email from Monday to Friday from 10 a.m. to 5 p.m.</p>
					  <?php elseif(ICL_LANGUAGE_CODE =='fr'):?>
					  <p class="text-muted mt-0 mb-1">Notre service client est à votre disposition par mail du lundi au vendredi de 10h à 17h.</p>
					   <?php endif; ?>
				  	
					<a href="mailto:sav@shop.richardorlinski.fr" class="d-block">sav@shop.richardorlinski.fr</a>
					<a href="mailto:info@shop.richardorlinski.fr" class="d-block">info@shop.richardorlinski.fr</a>
				  </div>			      
			      <div class="col-md-3 col-12 my-md-2 my-4">
					<div class="col-md-12 row align-items-center mx-0 p-0">
					<div class="col-md-12 row  mx-0 px-0 align-items-center p-0">
						<div class="col-1 d-flex align-items-center justify-content-center p-0 py-1">
							<img src="<?php echo get_template_directory_uri();?>/img/truck.svg" class="" width="30px">
						</div>
						<div class="col-11">
							<?php if(ICL_LANGUAGE_CODE=='en'):?>
								<p class="m-0 text-muted">Delivery guaranteed by Colissimo<br>6 to 8 working days delay</p>
							<?php else:?>
								<p class="m-0 text-muted">Livraison assurée par Colissimo<br>Délai de 6 à 8 jours ouvrés</p>
							<?php endif;?>
						</div>
					</div>
					<div class="col-md-12 row mx-0 px-0 align-items-center p-0">
						<div class="col-1 d-flex align-items-center justify-content-center p-0 py-1">
							<img src="<?php echo get_template_directory_uri();?>/img/card.svg" class="" width="30px">
						</div>
						<div class="col-11">
							<?php if(ICL_LANGUAGE_CODE=='en'):?>
								<p class="m-0 text-muted">Secure payment<br>Credit card and Paypal</p>
							<?php else:?>
								<p class="m-0 text-muted">Paiement sécurisé<br>Carte bancaire et Paypal</p>
							<?php endif;?>
						</div>			
					</div>
				</div>			      	
			      </div>	
		      			 
				  <div class="col-md-3 my-md-2 my-4">
				  	  <?php if(ICL_LANGUAGE_CODE =='en'): ?>
					  <h5 class="widget-title">Subscribe for newsletter</h5>
					  <?php acfe_form('subscribe-en'); ?>
					  <?php elseif(ICL_LANGUAGE_CODE =='fr'):?>
					  <h5 class="widget-title">S’inscrire à la newsletter</h5>
					  <?php acfe_form('subscribe-fr'); ?>
					   <?php endif; ?>
				  </div>

			      <div class="col-md-2 my-md-2 my-4 mx-auto text-md-left text-center">
				   <?php if(ICL_LANGUAGE_CODE =='en'): ?>
				  <h5 class="widget-title">Follow</h5>
				  <?php elseif(ICL_LANGUAGE_CODE =='fr'):?>
				  <h5 class="widget-title">Suivre</h5>
				   <?php endif; ?>
				      <div class="social-media-icons">
						<a target="_blank" href="https://instagram.com/orlinskishop/" class="icon fb-icon"><img src="<?php echo get_template_directory_uri(); ?>/img/icon-instagram.svg" style="height: 20px;"></a>
						<a target="_blank" href="https://www.facebook.com/richardorlinski.fr/" class="icon in-icon ml-3"><img src="<?php echo get_template_directory_uri(); ?>/img/icon-facebook.svg" style="height: 20px;"></a>
						<a target="_blank" href="https://www.youtube.com/channel/UC7k-X2MFn9LjMLBHqhcObvg" class="icon yt-icon ml-3"><img src="<?php echo get_template_directory_uri(); ?>/img/icon-youtube.svg" style="height: 20px;"></a>
					
						</div>
			      </div>
	


		</div><!-- row end -->

	</div><!-- container end -->

</div><!-- wrapper end -->

</div><!-- #page we need this extra closing tag here -->

<?php wp_footer(); ?>

</body>

</html>

