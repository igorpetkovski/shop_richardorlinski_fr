module.exports = function(grunt) {
  require('jit-grunt')(grunt);

  grunt.initConfig({
    less: {
      development: {
        options: {
          compress: true,
          yuicompress: false,
          optimization: 2,
          sourceMap: true,
        },
        files: {
          'css/main.css': 'less/main.less',
         
        }
      }
    },

    watch: {
      grunt: { files: ['Gruntfile.js'] },

      styles: {
        files: [
          'less/**/*.less',
          
        ],
        tasks: ['less'],
      },
    }
  });

  grunt.registerTask('default', ['less', 'watch']);
};