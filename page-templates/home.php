<?php
/**
 * Template Name: Home
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;
if(current_user_can('administrator') || current_user_can('shop_manager')):
	get_header('2');
else:
get_header();
endif;
?>
<?php while ( have_posts() ) : the_post(); ?>
<div class="container-fluid mx-0 px-0">
	<!-- section 1 -->
	<div class="row mx-0" id="hero-section">
		<div class="col-md-12 px-0">
			<div class="row mx-0 px-0">
				<?php if(current_user_can('administrator') || current_user_can('shop_manager') ): ?>
				<?php else: ?>
				<div class="col-md-12">
				<div class="col-md-3 col-9 mx-auto">
					<img src="<?php echo get_template_directory_uri();?>/img/logoNew.svg" class="img-fluid home-logo">
				</div>
				</div>
				<?php endif; ?>


				<?php if(current_user_can('administrator') || current_user_can('shop_manager')): ?>
				<?php else: ?>
				<div class="col-md-12 px-0 mt-2" style="background-image:url('<?php $image = get_field('image'); echo wp_get_attachment_url($image);?>'); background-size:cover; background-position:center; background-repeat: no-repeat; height: 650px;">
				
				<div class="col-md-12 mt-md-5 mt-3 px-0">
				<div class="col-md-12 text-center mx-auto">
					<h1 class="text-uppercase text-white"><?php the_field('title');?></h1>
				</div>
				</div>

				</div>
				<?php endif; ?>
				
				<?php if(current_user_can('administrator') || current_user_can('shop_manager')): ?>
					<div class="col-md-12 px-0 mx-0">
						<?php 
						if(has_flexible('carousel')):
							the_flexible('carousel');
						endif;
						?>
					</div>
				
				<?php else: ?>
				<?php endif; ?>
				
				
				<?php if(current_user_can('administrator') || current_user_can('shop_manager')): ?>
				<?php else: ?>
 				<div class="col-md-12">
					<div class="col-lg-4 col-md-6 mx-auto px-0" style="position: relative; top: -25px;">
						<a href="<?php echo get_permalink(get_field('product'))?>" class="btn btn-primary btn-block btn-lg"><?php the_field('add_to_cart'); ?></a>
					</div>
				</div> 
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>		


<div class="container-fluid mx-0 px-0">
	<!-- section 2 -->
	<div class="row mx-0 my-md-5 my-5">
		<div class="col-md-12 px-md-2 px-0">
		<div class="container">

			<div class="row mx-0">
				<div class="col-md-12 my-md-3 my-1">
				<div class="col-md-12 mx-auto px-0">
					<h2 class="text-uppercase text-center"><?php the_field('about_title');?></h2>
				</div>
				</div>

				<div class="col-md-12 my-md-3 my-1">
				<div class="col-md-10  mx-auto px-0">
					<p><?php the_field('about_text');?></p>
				</div>
				</div>
			</div>

			<div class="col-lg-4 col-md-6 mx-auto px-0">			
				<button type="button" class="btn btn-dark btn-block btn-lg mx-auto" data-toggle="modal" data-target="#orlinskiShopConcept">
				  <?php the_field('about_title_2');?>
				</button>
			</div>

			<!-- Modal -->
			<div class="modal fade" id="orlinskiShopConcept" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			  <div class="modal-dialog" role="document" style="margin-top:0px; margin-bottom:0px; display: flex; align-items: center;justify-content: center; height: 100%;
			  width:100%; max-width: 600px;">
			    <div class="modal-content">
			      <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
			      </div>
			      <div class="modal-body">
						<div class="row mx-0">
							<div class="col-md-12 my-md-3 my-1">
							<div class="col-md-12 mx-auto px-0">
								<h3 class="text-uppercase text-center text-primary"><?php the_field('about_title_2');?></h3>
							</div>
							</div>

							<div class="col-md-12 my-md-3 my-1">
							<div class="col-md-10  mx-auto px-0 text-center">
								<h6 class="text-uppercase"><?php the_field('about_text_2');?></h6>
							</div>
							</div>
						</div>
			      </div>
			    </div>
			  </div>
			</div>
			<!-- modal end -->
		</div>
		</div>
	</div>
	<!-- section 2 end -->

	<!-- section 3 -->
	<div class="row mx-0 my-md-5">
		<?php $images = get_field('images');
		foreach($images as $image): ?>
			<div class="col-md-4 px-0">
				<div class="image-wrapper">
				<?php 
					$image = $image['image'];
					$size = 'full';
					if($image):
						echo wp_get_attachment_image($image,$size,'',array('class'=>'img-fluid'));
					endif; 
				?>
				</div>
			</div> 
					
		<?php endforeach;
		?>
	</div>
	<!-- section 3 end -->	

				
				
	<?php if(current_user_can('administrator') || current_user_can('shop_manager') ): ?>
				

	<!-- Products Section -->
	<div class="container">
		
			<?php
	if( have_rows('add_new_product_section') ):
   $i=0; while( have_rows('add_new_product_section') ) : the_row(); $i++; ?>

    	<div class="row mx-0 justify-content-center" style="position: relative;">
       			<h5 class="product-writing text-uppercase"> <?php  the_sub_field('category'); ?> </h5>
    	<?php 
		$rows = get_sub_field('add_products');
		$queued_products = array();
		if($rows):
			foreach($rows as $row):

				array_push($queued_products,$row['product']);

			endforeach; endif; ?>

    	<?php
			$args = array(
			    'post_type' => 'product',
			    'posts_per_page' => 3,
			    'post__in' => $queued_products,
			    'orderby'  => 'post__in'
			    );
			$loop = new WP_Query( $args );
			if ( $loop->have_posts() ):
			    while ( $loop->have_posts() ) : $loop->the_post(); ?>
			    	
			<div class="col-md-4 p-2 content-product">
			<div <?php wc_product_class( 'border text-center p-3', $product ); ?>>
				<?php echo do_shortcode('[yith_wcwl_add_to_wishlist]');?>
				<?php

				do_action( 'woocommerce_before_shop_loop_item' );

				do_action( 'woocommerce_before_shop_loop_item_title' );


				do_action( 'woocommerce_shop_loop_item_title' );


				do_action( 'woocommerce_after_shop_loop_item_title' );

				// do_action( 'woocommerce_after_shop_loop_item' );
				?>
			</div>
			</div>



			<?php endwhile; endif; wp_reset_postdata(); ?>

       				<?php if($i==2): ?>

					<div class="col-md-4 p-2 see-more-product">
						<div class="see-more-wrapper">
							<h3><?php the_field('btn_title'); ?></h3>

							<a style="z-index: 2" href="<?php echo get_field('button')['url']; ?>" class="btn btn-primary"><?php echo get_field('button')['title']; ?></a>
						</div>
					</div>

					<?php endif; ?>



		</div>
   
    <?php endwhile; endif; ?>
	</div>


	<?php else: ?>

	<?php endif; ?>







	<!-- Products Section END -->


	<!-- section 4 -->
	<div class="row mx-0 mt-md-5 mb-md-4 mt-3 mb-4">
		<div class="col-md-12 px-md-2 px-0">
		<div class="container">
			<div class="row mx-0">
				<div class="col-md-12 my-3 px-md-2 px-0">
				<div class="col-md-10 mx-auto px-md-2 px-0">
					<div class="video position-relative">
					<video playsinline autoplay muted loop id="video" style="width: 100%;">
						<source src="<?php the_field('video'); ?>" type="video/mp4">
					</video>
					<span type="button" class="video-muted"></span>
					</div>
				</div>
				</div>
			</div>
		</div>
		</div>
	</div>
	<!-- section 4 end -->

	<!-- section 4 -->
	<div class="row mx-0 mt-md-4 mb-md-5 mt-4 mb-3">
		<div class="col-md-12 px-md-2 px-0">
		<div class="container">
			<div class="row mx-0">
				<div class="col-md-12 my-3 px-md-2 px-0">
				<div class="col-md-10 mx-auto px-md-2 px-0">
					<?php if(ICL_LANGUAGE_CODE == 'en'):
					 echo wp_get_attachment_image( 4697, 'full', '',array('class'=>'img-fluid') );
					elseif(ICL_LANGUAGE_CODE == 'fr'):
					 echo wp_get_attachment_image( 4695,'full', '',array('class'=>'img-fluid'));
					 endif;?>
				</div>
				</div>
			</div>
		</div>
		</div>
	</div>
	<!-- section 4 end -->

	<!-- section 5 -->
	<div class="row mx-0  py-5" id="app" style="background-image:url('<?php the_field('app_image');?>');">
		<div class="col-md-12 my-md-5 px-md-2 px-0">
		<div class="container my-md-5">
			<div class="row mx-0">
				<div class="col-md-10 mx-auto my-4">
				<div class="col-md-7 offset-md-5 my-3 col-9">
					<h5 class="text-primary"><?php the_field('app_above_title');?></h5>
					<h4 class="text-white app-text"><?php the_field('app_text');?></h4>
				</div>
				</div>
			</div>
		</div>
		</div>		
	</div>
	<!-- section 5 end -->	


	<!-- section 3 -->
	<div class="row mx-0">
		<?php $images = get_field('footer_images');
		foreach($images as $image): ?>
			<div class="col-md-3 col-6 px-0">
				<div class="image-wrapper">
				<?php 
					$image = $image['image'];
					$size = 'full';
					if($image):
						echo wp_get_attachment_image($image,$size,'',array('class'=>'img-fluid'));
					endif; 
				?>
				</div>
			</div> 
					
		<?php endforeach;
		?>
	</div>
	<!-- section 3 end -->	



</div>
<?php endwhile; // end of the loop. ?>
<?php get_footer(); ?>
