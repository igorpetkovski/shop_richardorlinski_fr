<?php
/**
 * Template Name: COMING-SOON
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;
?>

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
<link rel='stylesheet' id='main-css-css'  href='https://shop.richardorlinski.fr/wp-content/themes/orlinski/css/main.css?ver=1.01' type='text/css' media='all' />
		<div class="marquee">
		  <div>
		  </div>
		</div>

		<div class="container">
			<div class="row mx-0 align-items-center h-100">

				

				<div class="col-md-12 my-3 px-0">
					<div class="col-md-4 my-4 col-9 mx-auto">
					<img src="https://shop.richardorlinski.fr/wp-content/themes/orlinski/img/logoNew.svg" class="img-fluid">
				</div>
				<div class="col-md-12 text-center mx-auto">
					<h1 class>COMING (VERY) SOON</h1>
				</div>
				</div>

			</div>
		</div>






    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>


