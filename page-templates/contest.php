<?php
/**
 * Template Name: Contest
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();

$email = $_GET['email'];

?>



<div class="wrapper mb-5" id="full-width-page-wrapper">

	<div class="container" id="content">

		<div class="row">

			<div class="col-md-10 mx-auto content-area" id="primary">

				<main class="site-main" id="main" role="main">

					<?php while ( have_posts() ) : the_post(); ?>
						<?php if(email_exists($email)):?>
							<?php 
							$user = get_user_by('email',$email);
									add_user_meta($user->ID,'draw',1);
									add_user_meta($user->ID,'ip',$_SERVER['REMOTE_ADDR']);

								if(get_field('mailsent','user_'.$user->ID)!=1){
								// send email
							    // Define a constant to use with html emails
								define("HTML_EMAIL_HEADERS", array("From: E-Shop Richard Orlinski <info@shop.richardorlinski.fr>". "\r\n", "Content-Type: text/html; charset=UTF-8"));

								// @email - Email address of the reciever
								// @subject - Subject of the email
								// @heading - Heading to place inside of the woocommerce template
								// @message - Body content (can be HTML)
								if(ICL_LANGUAGE_CODE=='fr'){
									$heading = 'Merci pour votre inscription ! ';
									$subject = 'Merci pour votre inscription ! ';
									$message ='Votre inscription pour le tirage au sort de la Full Black Kong Kase a bien été pris en compte. <br>
											 <br>Vous recevrez un mail dès que le tirage au sort sera terminé pour savoir si vous avez gagné. <br><<br>Fin du tirage au sort : 21 juillet à 18h. ';
								}
								else if(ICL_LANGUAGE_CODE=='en'){
								$heading = 'Thank you for your registration !';
								$subject = 'Thank you for your registration !';
										$message ='Your registration for the Full Black Kong Kase draw has been taken into account. <br>
											 <br>You will receive an email as soon as the draw is over to find out if you have won. <br><<br>End of the draw: July 21 at 6 p.m. ';
								}
								  // Get woocommerce mailer from instance
								  $mailer = WC()->mailer();

								  // Wrap message using woocommerce html email template
								  $wrapped_message = $mailer->wrap_message($heading, $message);

								  // Create new WC_Email instance
								  $wc_email = new WC_Email;

								  // Style the wrapped message with woocommerce inline styles
								  $html_message = $wc_email->style_inline($wrapped_message);

								  // Send the email using wordpress mail function
								  wp_mail( $email, $subject, $html_message, HTML_EMAIL_HEADERS );
								 add_user_meta($user->ID,'mailsent',1);
								}


							?>
							<div class="row mx-0 align-items-center">
									<div class="col-md-7">
									<header class="entry-header">
										<h1 class="entry-title text-center text-uppercase my-md-3"><?php the_field('thank_you_title');?></h1>
									</header><!-- .entry-header -->
									<div class="mb-3">
										<p><?php the_field('thank_you_text');?></p>
									</div>
									<div class="my-3 col-md-5 mx-auto">
										<a href="<?php echo get_site_url();?>" class="btn btn-primary btn-block"><?php if(ICL_LANGUAGE_CODE == 'fr'):
											echo 'RETOURNER SUR LE SITE';
											
											elseif(ICL_LANGUAGE_CODE == 'en'):
											echo 'RETURN TO WEBSITE';
											endif;?>
											</a>
									</div>	
									</div>

									<div class="col-md-5">
										<img src="<?php echo get_template_directory_uri();?>/img/kong-baril-<?php echo ICL_LANGUAGE_CODE; ?>.png" class="img-fluid">
									</div>
								</div>	
							<?php else:?>
								<div class="row mx-0 align-items-start">
									<div class="col-md-6 ">
									<div class="my-4 col-md-12 mx-auto">
										<h4 class="text-primary text-uppercase font-weight-light mb-4"><?php the_field('steps_title');?></h4>
										<p class="text-muted"><?php the_field('explanation');?></p>
										<div id="steps" class="my-4">
											<p><?php the_field('steps_subtitle');?></p>
											<?php
											$steps = get_field('steps');
											foreach($steps as $step):?>
												<div class="step <?php if($step['active']=='1'): echo 'active'; endif;?>">
												<h6 class="text-uppercase"><?php echo $step['title'];?></h6>
												<p class="mb-4"><?php echo $step['text'];?></p>
												</div>
											<?php endforeach;?>
											<p class="small"><?php the_field('steps_description');?></p>
										</div>
									</div>	
									</div>

									<div class="col-md-6">
										<div class="my-4 row mx-0" id="customer_login">
											<div class="col-md-12 mx-auto">
												<h4 class="text-primary text-uppercase font-weight-light mb-4"><?php esc_html_e( 'Register', 'woocommerce' ); ?></h4>

												<?php do_action( 'woocommerce_before_customer_login_form' ); ?>

												<form method="post" class="woocommerce-form woocommerce-form-register register row mx-0 " <?php do_action( 'woocommerce_register_form_tag' ); ?> >

													<?php do_action( 'woocommerce_register_form_start' ); ?>

													<?php if ( 'no' === get_option( 'woocommerce_registration_generate_username' ) ) : ?>

														<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide col-md-12">
															<label for="reg_username"><?php esc_html_e( 'Username', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
															<input type="text" class="woocommerce-Input woocommerce-Input--text form-control input-text" name="username" id="reg_username" autocomplete="username" value="<?php echo ( ! empty( $_POST['username'] ) ) ? esc_attr( wp_unslash( $_POST['username'] ) ) : ''; ?>" /><?php // @codingStandardsIgnoreLine ?>
														</p>

													<?php endif; ?>

													<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide col-md-12">
														<label for="reg_email"><?php esc_html_e( 'Email address', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
														<input type="email" class="woocommerce-Input woocommerce-Input--text form-control input-text" name="email" id="reg_email" autocomplete="email" value="<?php echo ( ! empty( $_GET['email'] ) ) ? esc_attr( wp_unslash( $_GET['email'] ) ) : ''; ?>" /><?php // @codingStandardsIgnoreLine ?>
													</p>

													<?php if ( 'no' === get_option( 'woocommerce_registration_generate_password' ) ) : ?>

														<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide col-md-12">
															<label for="reg_password"><?php esc_html_e( 'Password', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
															<input type="password" class="woocommerce-Input woocommerce-Input--text form-control input-text" name="password" id="reg_password" autocomplete="new-password" />
														</p>

													<?php else : ?>

														<p><?php esc_html_e( 'A password will be sent to your email address.', 'woocommerce' ); ?></p>

													<?php endif; ?>

													<?php do_action( 'woocommerce_register_form' ); ?>

													<p class="woocommerce-FormRow form-row col-md-12">
														<?php wp_nonce_field( 'woocommerce-register', 'woocommerce-register-nonce' ); ?>
														<button type="submit" class="btn btn-primary btn-block btn-lg" name="register" value="<?php esc_attr_e( 'Register', 'woocommerce' ); ?>"><?php esc_html_e( 'Register', 'woocommerce' ); ?></button>
													</p>

													<?php do_action( 'woocommerce_register_form_end' ); ?>

												</form>

											</div>
										</div>

										<?php do_action( 'woocommerce_after_customer_login_form' ); ?>										
									</div>
								</div>									
							<?php endif;?>


					<?php endwhile; // end of the loop. ?>

				</main><!-- #main -->

			</div><!-- #primary -->

		</div><!-- .row end -->

	</div><!-- #content -->

</div><!-- #full-width-page-wrapper -->

<?php get_footer(); ?>
