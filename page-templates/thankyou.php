<?php
/**
 * Template Name: Thank you
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();

?>



<div class="wrapper mb-5" id="full-width-page-wrapper">

	<div class="container" id="content">

		<div class="row">

			<div class="col-md-10 mx-auto content-area" id="primary">

				<main class="site-main" id="main" role="main">


					<?php while ( have_posts() ) : the_post(); ?>
						<div class="row mx-0 align-items-center">
						<div class="col-md-7">
						<header class="entry-header">
							<?php the_title( '<h1 class="entry-title text-center text-uppercase my-md-3">', '</h1>' ); ?>

						</header><!-- .entry-header -->
						<div class="mb-3">
							<?php the_content();?>
						</div>
						<div class="my-3 col-md-5 mx-auto">
							<a href="<?php echo get_site_url();?>" class="btn btn-primary btn-block"><?php if(ICL_LANGUAGE_CODE == 'fr'):
								echo 'RETOURNER SUR LE SITE';
								
								elseif(ICL_LANGUAGE_CODE == 'en'):
								echo 'RETURN TO WEBSITE';
								endif;?>
									
								</a>
						</div>	
						</div>

						<div class="col-md-5 ">
							<img src="<?php echo get_template_directory_uri();?>/img/kong-baril-<?php echo ICL_LANGUAGE_CODE; ?>.png" class="img-fluid">
						</div>
						</div>

					<?php endwhile; // end of the loop. ?>

				</main><!-- #main -->

			</div><!-- #primary -->

		</div><!-- .row end -->

	</div><!-- #content -->

</div><!-- #full-width-page-wrapper -->

<?php get_footer(); ?>
