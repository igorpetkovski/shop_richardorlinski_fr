<?php
/**
 * Template Name: FAQ
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();

?>



<div class="wrapper mb-5" id="full-width-page-wrapper">

	<div class="container" id="content">

		<div class="row">

			<div class="col-md-10 mx-auto content-area" id="primary">

				<main class="site-main" id="main" role="main">


					<?php while ( have_posts() ) : the_post(); ?>

						<header class="entry-header">
							<?php the_title( '<h1 class="entry-title text-center text-uppercase my-md-5">', '</h1>' ); ?>

						</header><!-- .entry-header -->

						<?php
							if( have_rows('faq') ):
							    while ( have_rows('faq') ) : the_row(); ?>

							  <div class="accordion">
							    <div class="accordion-item">
							      <a><?php the_sub_field('question');?></a>
							      <div class="content">
							        <?php the_sub_field('answer');?>
							      </div>
							    </div>
							  </div>
							  
							    <?php endwhile;
							endif;

							?>

					<?php endwhile; // end of the loop. ?>

				</main><!-- #main -->

			</div><!-- #primary -->

		</div><!-- .row end -->

	</div><!-- #content -->

</div><!-- #full-width-page-wrapper -->

<?php get_footer(); ?>
