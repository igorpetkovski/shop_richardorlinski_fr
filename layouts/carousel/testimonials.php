<?php
/**
 * Carousel - Images
 *
 */
?>
<div class="testimonials-wrapper container-fluid mx-0 px-0 py-5" 
style=" background-image:url('<?php the_sub_field('background_image')?>');
        background-color:<?php the_sub_field('background_color');?>">

<div class="container">

  <div class="col-md-6 mx-auto my-3">
    <h3 class="text-center section-title yellow-splash-thick"><?php the_sub_field('title');?></h3>
  </div>
  <div class="slider" id="testimonials-slider">
    <?php $items = get_sub_field('items');
    foreach($items as $item):?>
      <div class="single-carousel-item-wrapper">
          <div class="row mx-0 bg-grey p-4 box-shadow">
          <div class="col-md-6 row mx-0">
            <?php for($i=0; $i< $item['rate']; $i++):?>
                <img src="<?php echo get_template_directory_uri();?>/img/star.svg" width="20px">
            <?php endfor;?>
          </div>
          <div class="col-md-6">
            <h5 class="text-right"><?php echo $item['title'];?></h5>
          </div>
          <div class="col-md-12 my-3">
            <?php echo $item['text'];?>
          </div>
        </div>
      </div>
    <?php endforeach;?>
  </div>

</div>
</div>



<script type="text/javascript">

jQuery(window).on('load',function(){
 jQuery('#testimonials-slider').slick({
    slidesToShow: "<?php echo get_sub_field('columns');?>",
    slidesToScroll: 1,
    rows:"<?php echo get_sub_field('rows');?>",
    infinite:true,
    arrows: true,
    centerMode:"<?php echo get_sub_field('center_mode');?>",
    focusOnSelect:true,
     responsive: [

      {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 576,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
      // You can unslick at a given breakpoint now by adding:
      // settings: "unslick"
      // instead of a settings object
    ]
   
  });
});

</script>