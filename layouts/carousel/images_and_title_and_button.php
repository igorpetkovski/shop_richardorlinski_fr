<?php
/**
 * Carousel - Images
 *
 */
?>
<div class="images-and-title-wrapper mx-0 px-0" style=" background-color:<?php the_sub_field('background_color');?>">
  <div class="slider" id="images-and-title-and-button-slider">
    <?php $items = get_sub_field('items');
    foreach($items as $item):?>
                <?php 
          $image = $item['image'];
          $size = 'full'; // (thumbnail, medium, large, full or custom size)
          ?>
      <div class="single-carousel-item-wrapper d-flex align-items-center" style="background-image:url('<?php echo wp_get_attachment_url($image);?>'); background-size:cover; background-position:center; background-repeat: no-repeat; height: 650px;">
        <div class="single-carousel-item container">
          <div class="row <?php echo $item['content_align']; ?>">
            <div class="col-md-4">
          <h2 class="mikado-font text-uppercase" style="color: <?php echo $item['text_color']; ?>"><?php echo $item['title'];?></h2>
          <a href="<?php echo $item['button']['url']?>" class="btn btn-primary btn-block mt-3"><?php echo $item['button']['title']?></a>
          </div>
          </div>
        </div>
      </div>
    <?php endforeach;?>
  </div>
</div>

<script type="text/javascript">

jQuery(window).on('load',function(){
 jQuery('#images-and-title-and-button-slider').slick({
    slidesToShow: "<?php echo get_sub_field('columns');?>",
    slidesToScroll: 1,
    rows:"<?php echo get_sub_field('rows');?>",
    infinite:true,
    arrows: true,
    centerMode:"<?php echo get_sub_field('center_mode');?>",
    focusOnSelect:true,
     responsive: [

      {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 576,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
      // You can unslick at a given breakpoint now by adding:
      // settings: "unslick"
      // instead of a settings object
    ]
   
  });
});

</script>