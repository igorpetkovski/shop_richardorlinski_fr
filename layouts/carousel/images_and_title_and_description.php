<?php
/**
 * Carousel - Images
 *
 */
?>
<div class="images-and-title-and-description-wrapper container-fluid mx-0 px-0 py-5" 
style=" background-image:url('<?php the_sub_field('background_image')?>');
        background-color:<?php the_sub_field('background_color');?>">

<div class="container">


<div class="slider" id="images-and-title-and-description-slider">
  <?php $items = get_sub_field('items');
  foreach($items as $item):?>
    <div class="single-carousel-item-wrapper">
      <div class=" bg-white">
        <h4 class="mikado-bold text-center item-title"><?php echo $item['title'];?></h4>
        <?php 
        $image = $item['image'];
        $size = 'full'; // (thumbnail, medium, large, full or custom size)
        if( $image ) {
            echo wp_get_attachment_image( $image, $size,'',array('class'=>'img-fluid') );
        }
        ?>
        <h6 class="text-center item-description"><?php echo $item['description']; ?></h6>
      </div>
    </div>
  <?php endforeach;?>
</div>

</div>
</div>
<script type="text/javascript">

jQuery(window).on('load',function(){
 jQuery('#images-and-title-and-description-slider').slick({
    slidesToShow: "<?php echo get_sub_field('columns');?>",
    slidesToScroll: 1,
    rows:"<?php echo get_sub_field('rows');?>",
    infinite:true,
    arrows: true,
    centerMode:"<?php echo get_sub_field('center_mode');?>",
    focusOnSelect:true,
     responsive: [

      {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 576,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
      // You can unslick at a given breakpoint now by adding:
      // settings: "unslick"
      // instead of a settings object
    ]
   
  });
});

</script>