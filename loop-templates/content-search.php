<?php
/**
 * Search results partial template.
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;
?>

<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">

	<header class="entry-header border-bottom my-3">

		<?php
		the_title(
			sprintf( '<h3 class="entry-title text-dark"><a href="%s" rel="bookmark" class="text-dark">', esc_url( get_permalink() ) ),
			'</a></h3>'
		);
		?>

		<?php if ( 'post' == get_post_type() ) : ?>

	

		<?php endif; ?>

	</header><!-- .entry-header -->



</article><!-- #post-## -->
