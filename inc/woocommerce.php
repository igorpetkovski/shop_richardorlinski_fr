<?php
/**
 * Add WooCommerce support
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

add_action( 'after_setup_theme', 'understrap_woocommerce_support' );
if ( ! function_exists( 'understrap_woocommerce_support' ) ) {
	/**
	 * Declares WooCommerce theme support.
	 */
	function understrap_woocommerce_support() {
		add_theme_support( 'woocommerce' );

		// Add New Woocommerce 3.0.0 Product Gallery support.
		add_theme_support( 'wc-product-gallery-lightbox' );
		add_theme_support( 'wc-product-gallery-zoom' );
		add_theme_support( 'wc-product-gallery-slider' );

		// hook in and customizer form fields.
		add_filter( 'woocommerce_form_field_args', 'understrap_wc_form_field_args', 10, 3 );
	}
}

/**
 * First unhook the WooCommerce wrappers
 */
remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10 );
remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10 );

/**
 * Then hook in your own functions to display the wrappers your theme requires
 */
add_action( 'woocommerce_before_main_content', 'understrap_woocommerce_wrapper_start', 10 );
add_action( 'woocommerce_after_main_content', 'understrap_woocommerce_wrapper_end', 10 );
if ( ! function_exists( 'understrap_woocommerce_wrapper_start' ) ) {
	function understrap_woocommerce_wrapper_start() {
		$container = get_theme_mod( 'understrap_container_type' );
		echo '<div class="wrapper" id="woocommerce-wrapper">';
		echo '<div class="' . esc_attr( $container ) . '" id="content" tabindex="-1">';
		echo '<div class="row">';
		get_template_part( 'global-templates/left-sidebar-check' );
		echo '<main class="site-main" id="main">';
	}
}
if ( ! function_exists( 'understrap_woocommerce_wrapper_end' ) ) {
	function understrap_woocommerce_wrapper_end() {
		echo '</main><!-- #main -->';
		get_template_part( 'global-templates/right-sidebar-check' );
		echo '</div><!-- .row -->';
		echo '</div><!-- Container end -->';
		echo '</div><!-- Wrapper end -->';
	}
}


/**
 * Filter hook function monkey patching form classes
 * Author: Adriano Monecchi http://stackoverflow.com/a/36724593/307826
 *
 * @param string $args Form attributes.
 * @param string $key Not in use.
 * @param null   $value Not in use.
 *
 * @return mixed
 */
if ( ! function_exists( 'understrap_wc_form_field_args' ) ) {
	function understrap_wc_form_field_args( $args, $key, $value = null ) {
		// Start field type switch case.
		switch ( $args['type'] ) {
			/* Targets all select input type elements, except the country and state select input types */
			case 'select':
				// Add a class to the field's html element wrapper - woocommerce
				// input types (fields) are often wrapped within a <p></p> tag.
				$args['class'][] = 'form-group';
				// Add a class to the form input itself.
				$args['input_class']       = array( 'form-control', 'input-lg' );
				$args['label_class']       = array( 'control-label' );
				$args['custom_attributes'] = array(
					'data-plugin'      => 'select2',
					'data-allow-clear' => 'true',
					'aria-hidden'      => 'true',
					// Add custom data attributes to the form input itself.
				);
				break;
			// By default WooCommerce will populate a select with the country names - $args
			// defined for this specific input type targets only the country select element.
			case 'country':
				$args['class'][]     = 'form-group single-country';
				$args['label_class'] = array( 'control-label' );
				break;
			// By default WooCommerce will populate a select with state names - $args defined
			// for this specific input type targets only the country select element.
			case 'state':
				// Add class to the field's html element wrapper.
				$args['class'][] = 'form-group';
				// add class to the form input itself.
				$args['input_class']       = array( '', 'input-lg' );
				$args['label_class']       = array( 'control-label' );
				$args['custom_attributes'] = array(
					'data-plugin'      => 'select2',
					'data-allow-clear' => 'true',
					'aria-hidden'      => 'true',
				);
				break;
			case 'password':
			case 'text':
			case 'email':
			case 'tel':
			case 'number':
				$args['class'][]     = 'form-group';
				$args['input_class'] = array( 'form-control', 'input-lg' );
				$args['label_class'] = array( 'control-label' );
				break;
			case 'textarea':
				$args['input_class'] = array( 'form-control', 'input-lg' );
				$args['label_class'] = array( 'control-label' );
				break;
			case 'checkbox':
				$args['label_class'] = array( 'custom-control custom-checkbox' );
				$args['input_class'] = array( 'custom-control-input', 'input-lg' );
				break;
			case 'radio':
				$args['label_class'] = array( 'custom-control custom-radio' );
				$args['input_class'] = array( 'custom-control-input', 'input-lg' );
				break;
			default:
				$args['class'][]     = 'form-group';
				$args['input_class'] = array( 'form-control', 'input-lg' );
				$args['label_class'] = array( 'control-label' );
				break;
		} // end switch ($args).
		return $args;
	}
}

if ( ! is_admin() && ! function_exists( 'wc_review_ratings_enabled' ) ) {
	/**
	 * Check if reviews are enabled.
	 *
	 * Function introduced in WooCommerce 3.6.0., include it for backward compatibility.
	 *
	 * @return bool
	 */
	function wc_reviews_enabled() {
		return 'yes' === get_option( 'woocommerce_enable_reviews' );
	}

	/**
	 * Check if reviews ratings are enabled.
	 *
	 * Function introduced in WooCommerce 3.6.0., include it for backward compatibility.
	 *
	 * @return bool
	 */
	function wc_review_ratings_enabled() {
		return wc_reviews_enabled() && 'yes' === get_option( 'woocommerce_enable_review_rating' );
	}
}

// remove imge zoom
function remove_image_zoom_support() {
    remove_theme_support( 'wc-product-gallery-zoom' );
}
add_action( 'wp', 'remove_image_zoom_support', 100 );


// tabs
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );
// meta
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );

// thank you title
add_filter( 'woocommerce_endpoint_order-received_title', 'misha_thank_you_title' );
function misha_thank_you_title( $old_title ){
 if(ICL_LANGUAGE_CODE =='en'){
	 return 'CONFIRMATION OF ORDER';
 }
 else{
	 return 'CONFIRMATION DE COMMANDE';
 }

}

// mini cart
remove_action( 'woocommerce_widget_shopping_cart_buttons', 'woocommerce_widget_shopping_cart_button_view_cart', 10 );
remove_action( 'woocommerce_widget_shopping_cart_buttons', 'woocommerce_widget_shopping_cart_proceed_to_checkout', 20 );

function my_woocommerce_widget_shopping_cart_button_view_cart() {
    echo '<a href="' . esc_url( wc_get_cart_url() ) . '" class="btn btn-outline-dark btn-sm btn-block">' . esc_html__( 'View cart', 'woocommerce' ) . '</a>';
}
function my_woocommerce_widget_shopping_cart_proceed_to_checkout() {
    echo '<a href="' . esc_url( wc_get_checkout_url() ) . '" class="btn btn-primary btn-sm btn-block">' . esc_html__( 'Checkout', 'woocommerce' ) . '</a>';
}
add_action( 'woocommerce_widget_shopping_cart_buttons', 'my_woocommerce_widget_shopping_cart_button_view_cart', 10 );
add_action( 'woocommerce_widget_shopping_cart_buttons', 'my_woocommerce_widget_shopping_cart_proceed_to_checkout', 20 );


// shipping delivery
/**
 * only copy opening php tag if needed
 * Displays shipping estimates for WC shipping rates
 */
function sv_shipping_method_estimate_label( $label, $method ) {

	$label .= '<br /><small>';

	switch (ICL_LANGUAGE_CODE) {
		case 'en':
			
			$delivery3='Estimated delivery: D+3 + D+5';
			$delivery4='Estimated delivery: D+2';
			break;
		
		default:
			
			$delivery3='Le délai indicatif est de J+3 à J+5';
			$delivery4='Le délai indicatif est de J+2';
			break;
	}
	switch ( $method->instance_id ) {
		case '2':
			// $label .= $delivery4;
			break;

		case '4':
			// $label .= $delivery4;
			break;

		case '7':case '8':case '9':case '10':case '11':
			// $label .= $delivery3;
			break;

		default: 
			// $label .= $delivery4;
	}
	
	$label .= '</small>';
	return $label;
}
add_filter( 'woocommerce_cart_shipping_method_full_label', 'sv_shipping_method_estimate_label', 10, 2 );

function sv_shipping_method_estimate_label_thankyou( $instance_id ) {
	$label = '';
	$label .= '<p>';

	switch (ICL_LANGUAGE_CODE) {
		case 'en':
			
			$delivery3='Estimated delivery: D+3 + D+5';
			$delivery4='Estimated delivery: D+2';
			break;
		
		default:
			
			$delivery3='Le délai indicatif est de J+3 à J+5';
			$delivery4='Le délai indicatif est de J+2';
			break;
	}
	switch ( $instance_id ) {
		case '2':
			$label .= $delivery4;
			break;

		case '4':
			$label .= $delivery4;
			break;

		case '7':case '8':case '9':case '10':case '11':
			$label .= $delivery3;
			break;

		default: 
			$label .= $delivery4;
	}
	
	$label .= '</p>';
	return $label;
}



/**
 * Automatically add product to cart on visit
 */
// add_action( 'template_redirect', 'add_product_to_cart' );
function add_product_to_cart() {
		if(ICL_LANGUAGE_CODE == 'fr'){
			$product_id = 433;
			$case_id = 199;
		}
		else{
		$product_id = 434; //replace with your own product id
		$case_id = 10;
		}
		$found = false;
		//check if product already in cart
		if ( sizeof( WC()->cart->get_cart() ) > 0 ) {
			 foreach ( WC()->cart->get_cart() as $cart_item_key => $values ) { 
				$_product = $values['data'];
				if ( $_product->get_id() == $product_id ){
					$found = true;
					$key = $cart_item_key;
				}
				if ( $_product->get_id() == $case_id ){
					$case_qty = $values['quantity'];
				}
			}
			// if product not found, add it
			if ( $found == false){
				WC()->cart->add_to_cart( $product_id, $case_qty );
			}
			else{
				if($case_qty){
				WC()->cart->set_quantity($cart_item_key, $case_qty);
				}
				else{
					WC()->cart->remove_cart_item($cart_item_key);
				}
			}
		} else {
			// if no products in cart, add it
			if($case_qty){
				WC()->cart->add_to_cart( $product_id, $case_qty );
			}
			else{
				WC()->cart->remove_cart_item($cart_item_key);
			}
		}
}


// CUSTOM STORE EXPORTER
function custom_woo_ce_order_fields( $fields ) {

	$fields[] = array(
		'name' => 'shipping_field',
		'label' => __( 'Shipping Field', 'woo_ce' ),
		'hover' => __( 'Static Field within functions.php', 'woo_ce' )
	);
	
	$fields[] = array(
		'name' => 'custom_quantity',
		'label' => __( 'Custom Quantity', 'woo_ce' ),
		'hover' => __( 'Static Field within functions.php', 'woo_ce' )
	);
		$fields[] = array(
		'name' => 'reference_number',
		'label' => __( 'Reference', 'woo_ce' ),
		'hover' => __( 'Static Field within functions.php', 'woo_ce' )
	);
				$fields[] = array(
		'name' => 'shipping_phone',
		'label' => __( 'Shipping Phone', 'woo_ce' ),
		'hover' => __( 'Static Field within functions.php', 'woo_ce' )
	);
						$fields[] = array(
		'name' => 'shipping_fax',
		'label' => __( 'Shipping Fax', 'woo_ce' ),
		'hover' => __( 'Static Field within functions.php', 'woo_ce' )
	);
			$fields[] = array(
		'name' => 'shipping_email',
		'label' => __( 'Shipping Email', 'woo_ce' ),
		'hover' => __( 'Static Field within functions.php', 'woo_ce' )
	);	
	return $fields;

}
add_filter( 'woo_ce_order_fields', 'custom_woo_ce_order_fields' );

function custom_woo_ce_order( $order, $order_id ) {
	
$shipping_method = @array_shift($order->get_shipping_methods());
$shipping_method_id = $shipping_method['instance_id'];

switch ($shipping_method_id) {
    case 2:
       $collisimo_code = 2;
        break;
	case 4:
       $collisimo_code = 7;
        break;
    case 15:
       $collisimo_code = 35;
        break;


	case 5:
       $collisimo_code = 42;
        break;
	case 3:
       $collisimo_code = 42;
        break;
	case 7:
       $collisimo_code = 44;
        break;
	case 8:
       $collisimo_code = 44;
        break;
	case 11:
       $collisimo_code = 44;
        break;
	case 8:
       $collisimo_code = 44;
        break;
	case 9:
       $collisimo_code = 44;
        break;        
	case 12:
       $collisimo_code = 44;

    case 13:
       $collisimo_code = 46;
    case 14:
       $collisimo_code = 46;       
        break;
    case 16:
       $collisimo_code = 46;       
        break;

	default:
		$collisimo_code = 505;	
}
	$order->shipping_field =$collisimo_code;
	$order->custom_quantity = intval(1);

	$order->shipping_phone = $order->get_billing_phone();
	$order->shipping_fax = $order->get_billing_phone();
	$order->shipping_email = $order->get_billing_email();
	return $order;

}
add_filter( 'woo_ce_order', 'custom_woo_ce_order', 10, 2 );



// /**
//  * Auto Complete all WooCommerce orders.
//  */
// add_action( 'woocommerce_thankyou', 'custom_woocommerce_auto_complete_order' );
// function custom_woocommerce_auto_complete_order( $order_id ) { 
//     if ( ! $order_id ) {
//         return;
//     }

//     $order = wc_get_order( $order_id );
//     $order->update_status( 'completed' );
// }

// ADD FIELDS TO WORDPRESS REGISTRATION FORM

// add_action( 'woocommerce_register_form', 'misha_add_register_form_field' );
 
function misha_add_register_form_field(){
 
	woocommerce_form_field(
		'first_name',
		array(
			'class'       => array('col-md-6'),
			'type'        => 'text',
			'required'    => true, // just adds an "*"
			'label'       => __('First name','woocommerce')
		),
		( isset($_POST['first_name']) ? $_POST['first_name'] : '' )
	);

		woocommerce_form_field(
		'last_name',
		array(
			'class'       => array('col-md-6'),
			'type'        => 'text',
			'required'    => true, // just adds an "*"
			'label'       => __('Last name','woocommerce')
		),
		( isset($_POST['last_name']) ? $_POST['last_name'] : '' )
	);

		woocommerce_form_field(
		'address_1',
		array(
			'class'       => array('col-md-12'),
			'type'        => 'text',
			'required'    => true, // just adds an "*"
			'label'       => __('Street address','woocommerce')
		),
		( isset($_POST['address_1']) ? $_POST['address_1'] : '' )
	);

			woocommerce_form_field(
		'address_2',
		array(
			'class'       => array('col-md-6'),
			'type'        => 'text',
			'required'    => false, // just adds an "*"
			'label'       => __('Apartment, suite etc..','woocommerce')
		),
		( isset($_POST['address_2']) ? $_POST['address_2'] : '' )
	);	

		woocommerce_form_field(
		'city',
		array(
			'class'       => array('col-md-6'),
			'type'        => 'text',
			'required'    => true, // just adds an "*"
			'label'       => __('City','woocommerce')
		),
		( isset($_POST['city']) ? $_POST['city'] : '' )
	);
	

		woocommerce_form_field(
		'postcode',
		array(
			'class'       => array('col-md-6'),
			'type'        => 'text',
			'required'    => true, // just adds an "*"
			'label'       => __('Postal code','woocommerce')
		),
		( isset($_POST['postcode']) ? $_POST['postcode'] : '' )
	);
		woocommerce_form_field(
		'phone',
		array(
			'class'       => array('col-md-6'),
			'type'        => 'text',
			'required'    => true, // just adds an "*"
			'label'       => __('Phone','woocommerce')
		),
		( isset($_POST['phone']) ? $_POST['phone'] : '' )
	);	

			woocommerce_form_field(
		'shipping_country',
		array(
			'class'       => array('col-md-12'),
			'input_class'=>array('form-control'),
			'type'        => 'country',
			'required'    => true, // just adds an "*"
			'label'       => __('Country','woocommerce')
		),
		( isset($_POST['shipping_country']) ? $_POST['shipping_country'] : '' )
	);	

 
}

// add_action( 'woocommerce_register_post', 'misha_validate_fields', 10, 3 );
function misha_validate_fields( $username, $email, $errors ) {
 	if(ICL_LANGUAGE_CODE == 'fr'){
 		$err = 'Tous les champs sont requis';
 	}
 	else {
 		$err = 'All fields are required';
 	}
	if
	 ( 
	   empty( $_POST['first_name'] )
	|| empty( $_POST['last_name'] )
	|| empty( $_POST['address_1'] )
	|| empty( $_POST['city'] )
	|| empty( $_POST['postcode'] )
	|| empty( $_POST['phone'] )
	|| empty( $_POST['shipping_country'] )
	 ) {
		$errors->add( 'first_name_error', $err );
	}
	
 
}

// add_action( 'woocommerce_created_customer', 'misha_save_register_fields' );
 
function misha_save_register_fields( $customer_id ){
 
	if ( isset( $_POST['first_name'] ) ) {
		update_user_meta( $customer_id, 'billing_first_name', wc_clean( $_POST['first_name'] ) );
		update_user_meta( $customer_id, 'shipping_first_name', wc_clean( $_POST['first_name'] ) );
		update_user_meta( $customer_id, 'first_name', wc_clean( $_POST['first_name'] ) );
	}
		if ( isset( $_POST['last_name'] ) ) {
		update_user_meta( $customer_id, 'billing_last_name', wc_clean( $_POST['last_name'] ) );
		update_user_meta( $customer_id, 'shipping_last_name', wc_clean( $_POST['last_name'] ) );
		update_user_meta( $customer_id, 'last_name', wc_clean( $_POST['last_name'] ) );

	}
		if ( isset( $_POST['address_1'] ) ) {
		update_user_meta( $customer_id, 'billing_address_1', wc_clean( $_POST['address_1'] ) );
		update_user_meta( $customer_id, 'shipping_address_1', wc_clean( $_POST['address_1'] ) );

	}
	if ( isset( $_POST['address_2'] ) ) {
		update_user_meta( $customer_id, 'billing_address_2', wc_clean( $_POST['address_2'] ) );
		update_user_meta( $customer_id, 'shipping_address_2', wc_clean( $_POST['address_2'] ) );
	}
		if ( isset( $_POST['city'] ) ) {
		update_user_meta( $customer_id, 'billing_city', wc_clean( $_POST['city'] ) );
		update_user_meta( $customer_id, 'shipping_city', wc_clean( $_POST['city'] ) );

	}
		if ( isset( $_POST['postcode'] ) ) {
		update_user_meta( $customer_id, 'billing_postcode', wc_clean( $_POST['postcode'] ) );
		update_user_meta( $customer_id, 'shipping_postcode', wc_clean( $_POST['postcode'] ) );

	}
			if ( isset( $_POST['phone'] ) ) {
		update_user_meta( $customer_id, 'billing_phone', wc_clean( $_POST['phone'] ) );
		update_user_meta( $customer_id, 'shipping_phone', wc_clean( $_POST['phone'] ) );

	}

			if ( isset( $_POST['shipping_country'] ) ) {
		update_user_meta( $customer_id, 'billing_country', wc_clean( $_POST['shipping_country'] ) );
		update_user_meta( $customer_id, 'shipping_country', wc_clean( $_POST['shipping_country'] ) );

		add_user_meta($customer_id,'draw',1);
		add_user_meta($customer_id,'ip',$_SERVER['REMOTE_ADDR']);

	}


		

 
}
// add_filter( 'woocommerce_registration_redirect', 'custom_redirection_after_registration', 10, 1 );




function custom_redirection_after_registration( $redirection_url ){
    // Change the redirection Url
    $redirection_url = get_site_url().'/contest-register/?lang='.ICL_LANGUAGE_CODE.'&email='.$_POST['email']; // Home page

    return $redirection_url; // Always return something



}
// add_filter( 'woocommerce_billing_fields' , 'custom_override_billing_fields' );
// add_filter( 'woocommerce_shipping_fields' , 'custom_override_shipping_fields' );
// // my account fields
// function custom_override_billing_fields( $fields ) {
//   // unset($fields['billing_country']);
//   unset($fields['billing_company']);
//   // unset($fields['billing_address_1']);
//   unset($fields['billing_address_2']);
//   unset($fields['billing_state']);
//   // unset($fields['billing_postcode']);
//   // unset($fields['billing_city']);
//   // unset($fields['billing_email']);
//   return $fields;
// }

// function custom_override_shipping_fields( $fields ) {
//   // unset($fields['shipping_country']);
//   unset($fields['shipping_company']);
//   // unset($fields['shipping_address_1']);
//   unset($fields['shipping_address_2']);
//   unset($fields['shipping_state']);
//   // unset($fields['shipping_postcode']);
//   // unset($fields['shipping_email']);
//   return $fields;
// }

add_filter('woocommerce_csv_import_limit_per_request', 'woocommerce_csv_import_limit_per_request');
function woocommerce_csv_import_limit_per_request()
{ return 10; }


// hide cancele button
add_filter( 'woocommerce_my_account_my_orders_actions', 'hide_cancel_on_my_account', 15, 2 );
function hide_cancel_on_my_account( $actions, $order ) {

			foreach ( $actions as $key => $value ) {
				if ( 'cancel' === $key  ) {
					unset( $actions[ $key ] );
				}
			}
			return $actions;
		}


		/**
 * Remove related products output
 */
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );


// pdf custom name filter
add_filter( 'wpo_wcpdf_format_document_number', 'wpo_wcpdf_custom_invoice_number', 10, 4 );
function wpo_wcpdf_custom_invoice_number( $formatted_number, $number, $document, $order ) {
	if ( $document->type == 'invoice' ) {
		$formatted_number = 'FAC_'.$order->get_order_number().'_'.$formatted_number;
	}
	return $formatted_number;
}