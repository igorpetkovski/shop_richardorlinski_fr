<?php

add_filter('acfe/form/load/newsletter-fr/form=subscribe-fr', 'my_form_custom_action_load', 10, 3);
add_filter('acfe/form/load/newsletter-en/form=subscribe-en', 'my_form_custom_action_load', 10, 3);

function my_form_custom_action_load($form, $post_id, $alias){
    /**
     * Change form redirection URL
     */
    $form['return'] = get_site_url().'/newsletter?lang='.ICL_LANGUAGE_CODE;
    
    /**
     * Return arguments
     * Note: Return false will hide the form
     */
    return $form;
    

}