<?php 
//  new  order status
function register_sent_order_status() {
    register_post_status( 'wc-sent', array(
        'label'                     => 'Shipped',
        'public'                    => true,
        'exclude_from_search'       => false,
        'show_in_admin_all_list'    => true,
        'show_in_admin_status_list' => true,
        'label_count'               => _n_noop( 'In Preparation (%s)', 'In preparation (%s)' )
    ) );
}
add_action( 'init', 'register_sent_order_status' );


// Add to list of WC Order statuses
function add_sent_to_order_statuses( $order_statuses ) {
 
    $new_order_statuses = array();
 
    // add new order status after processing
    foreach ( $order_statuses as $key => $status ) {
 
        $new_order_statuses[ $key ] = $status;
 
        if ( 'wc-processing' === $key ) {
            $new_order_statuses['wc-sent'] = 'In preparation';
        }
    }
 
    return $new_order_statuses;
}
add_filter( 'wc_order_statuses', 'add_sent_to_order_statuses' );



/*
 * Add your custom bulk action in dropdown
 * @since 3.5.0
 */
add_filter( 'bulk_actions-edit-shop_order', 'misha_register_bulk_action' ); // edit-shop_order is the screen ID of the orders page
 
function misha_register_bulk_action( $bulk_actions ) {
 
    $bulk_actions['mark_wc_sent'] = 'Mark as In preparation'; // <option value="mark_wc_sent">Mark awaiting shipment</option>
    return $bulk_actions;
 
}
 
/*
 * Bulk action handler
 * Make sure that "action name" in the hook is the same like the option value from the above function
 */
add_action( 'admin_action_mark_wc_sent', 'misha_bulk_process_custom_status' ); // admin_action_{action name}
 
function misha_bulk_process_custom_status() {
 
    // if an array with order IDs is not presented, exit the function
    if( !isset( $_REQUEST['post'] ) && !is_array( $_REQUEST['post'] ) )
        return;
 
    foreach( $_REQUEST['post'] as $order_id ) {
 
        $order = new WC_Order( $order_id );
        $order_note = 'Промена на статус:';
        $order->update_status( 'sent', $order_note, true ); // "misha-shipment" is the order status name (do not use wc-misha-shipment)
 
    }
 
    // of course using add_query_arg() is not required, you can build your URL inline
    $location = add_query_arg( array(
        'post_type' => 'shop_order',
        'marked_wc_sent' => 1, // markED_awaiting_shipment=1 is just the $_GET variable for notices
        'changed' => count( $_REQUEST['post'] ), // number of changed orders
        'ids' => join( $_REQUEST['post'], ',' ),
        'post_status' => 'all'
    ), 'edit.php' );
 
    wp_redirect( admin_url( $location ) );
    exit;
 
}
 
/*
 * Notices
 */
add_action('admin_notices', 'misha_custom_order_status_notices');
 
function misha_custom_order_status_notices() {
 
    global $pagenow, $typenow;
 
    if( $typenow == 'shop_order' 
     && $pagenow == 'edit.php'
     && isset( $_REQUEST['marked_wc_sent'] )
     && $_REQUEST['marked_wc_sent'] == 1
     && isset( $_REQUEST['changed'] ) ) {
 
        $message = sprintf( _n( 'Order status changed.', '%s order statuses changed.', $_REQUEST['changed'] ), number_format_i18n( $_REQUEST['changed'] ) );
        echo "<div class=\"updated\"><p>{$message}</p></div>";
 
    }
}


function confirmed_notifications($order_id, $checkout=null) {
    global $woocommerce;
    $order = new WC_Order( $order_id );
    if(get_post_meta($order_id, 'wpml_language', true)=='en'){
        $subject_field = 'shipping_email_subject_en';
        $heading_field = 'shipping_email_heading_en';
        $body_field = 'shipping_email_body_en';
    }elseif(get_post_meta($order_id, 'wpml_language', true)=='fr'){
        $subject_field = 'shipping_email_subject';
        $heading_field = 'shipping_email_heading';
        $body_field = 'shipping_email_body';
    }
    if( $order->status === 'sent' ) {
        $email = $order->get_billing_email();
        $subject = get_field($subject_field,'custom-emails');
        $heading = get_field($heading_field,'custom-emails');
        ob_start();
        the_field($body_field,'custom-emails');
        if(get_field('tracking_code',$order_id)){
            echo '<h4>Tracking # :'.get_field('tracking_code',$order_id).'</h4>';
        }
        do_action( 'woocommerce_email_order_details', $order, $sent_to_admin, $plain_text, $email );
        do_action( 'woocommerce_email_customer_details', $order, 0, '', $email );
        $output = ob_get_clean();
        send_email_woocommerce_style($email,$subject,$heading,$output);
    }
}
add_action("woocommerce_order_status_changed", "confirmed_notifications");



function custom_get_order_details($order, $sent_to_admin=0){    
$text_align = is_rtl() ? 'right' : 'left'; ?>
<div>
    <p><?php $plain_text = get_field('shipping_email_body','custom-emails'); ?></p>
</div>
<h2>
    <?php
    if ( $sent_to_admin ) {
        $before = '<a class="link" href="' . esc_url( $order->get_edit_order_url() ) . '">';
        $after  = '</a>';
    } else {
        $before = '';
        $after  = '';
    }
    /* translators: %s: Order ID. */
    echo wp_kses_post( $before . sprintf( __( '[Order #%s]', 'woocommerce' ) . $after . ' (<time datetime="%s">%s</time>)', $order->get_order_number(), $order->get_date_created()->format( 'c' ), wc_format_datetime( $order->get_date_created() ) ) );
    ?>
</h2>
<div style="margin-bottom: 40px;">
    <table class="td" cellspacing="0" cellpadding="6" style="width: 100%; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif;" border="1">
        <thead>
            <tr>
                <th class="td" scope="col" style="text-align:<?php echo esc_attr( $text_align ); ?>;"><?php esc_html_e( 'Product', 'woocommerce' ); ?></th>
                <th class="td" scope="col" style="text-align:<?php echo esc_attr( $text_align ); ?>;"><?php esc_html_e( 'Quantity', 'woocommerce' ); ?></th>
                <th class="td" scope="col" style="text-align:<?php echo esc_attr( $text_align ); ?>;"><?php esc_html_e( 'Price', 'woocommerce' ); ?></th>
            </tr>
        </thead>
        <tbody>
            <?php
            echo wc_get_email_order_items( // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
                $order,
                array(
                    'show_sku'      => $sent_to_admin,
                    'show_image'    => false,
                    'image_size'    => array( 32, 32 ),
                    'plain_text'    => $plain_text,
                    'sent_to_admin' => $sent_to_admin,
                )
            );
            ?>
        </tbody>
        <tfoot>
            <?php
            $item_totals = $order->get_order_item_totals();

            if ( $item_totals ) {
                $i = 0;
                foreach ( $item_totals as $total ) {
                    $i++;
                    ?>
                    <tr>
                        <th class="td" scope="row" colspan="2" style="text-align:<?php echo esc_attr( $text_align ); ?>; <?php echo ( 1 === $i ) ? 'border-top-width: 4px;' : ''; ?>"><?php echo wp_kses_post( $total['label'] ); ?></th>
                        <td class="td" style="text-align:<?php echo esc_attr( $text_align ); ?>; <?php echo ( 1 === $i ) ? 'border-top-width: 4px;' : ''; ?>"><?php echo wp_kses_post( $total['value'] ); ?></td>
                    </tr>
                    <?php
                }
            }
            if ( $order->get_customer_note() ) {
                ?>
                <tr>
                    <th class="td" scope="row" colspan="2" style="text-align:<?php echo esc_attr( $text_align ); ?>;"><?php esc_html_e( 'Note:', 'woocommerce' ); ?></th>
                    <td class="td" style="text-align:<?php echo esc_attr( $text_align ); ?>;"><?php echo wp_kses_post( nl2br( wptexturize( $order->get_customer_note() ) ) ); ?></td>
                </tr>
                <?php
            }
            ?>
        </tfoot>
    </table>
</div>

<?php }?>
