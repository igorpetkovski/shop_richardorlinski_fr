<?php 
// Register Custom Post Type
function subscriptions() {

	$labels = array(
		'name'                  => _x( 'Email Subscriptions', 'Post Type General Name', 'orlinski' ),
		'singular_name'         => _x( 'Email Subscription', 'Post Type Singular Name', 'orlinski' ),
		'menu_name'             => __( 'Email Subscriptions', 'orlinski' ),
		'name_admin_bar'        => __( 'Email Subscriptions', 'orlinski' ),
		'archives'              => __( 'Item Archives', 'orlinski' ),
		'attributes'            => __( 'Item Attributes', 'orlinski' ),
		'parent_item_colon'     => __( 'Parent Item:', 'orlinski' ),
		'all_items'             => __( 'All Items', 'orlinski' ),
		'add_new_item'          => __( 'Add New Item', 'orlinski' ),
		'add_new'               => __( 'Add New', 'orlinski' ),
		'new_item'              => __( 'New Item', 'orlinski' ),
		'edit_item'             => __( 'Edit Item', 'orlinski' ),
		'update_item'           => __( 'Update Item', 'orlinski' ),
		'view_item'             => __( 'View Item', 'orlinski' ),
		'view_items'            => __( 'View Items', 'orlinski' ),
		'search_items'          => __( 'Search Item', 'orlinski' ),
		'not_found'             => __( 'Not found', 'orlinski' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'orlinski' ),
		'featured_image'        => __( 'Featured Image', 'orlinski' ),
		'set_featured_image'    => __( 'Set featured image', 'orlinski' ),
		'remove_featured_image' => __( 'Remove featured image', 'orlinski' ),
		'use_featured_image'    => __( 'Use as featured image', 'orlinski' ),
		'insert_into_item'      => __( 'Insert into item', 'orlinski' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'orlinski' ),
		'items_list'            => __( 'Items list', 'orlinski' ),
		'items_list_navigation' => __( 'Items list navigation', 'orlinski' ),
		'filter_items_list'     => __( 'Filter items list', 'orlinski' ),
	);
	$args = array(
		'label'                 => __( 'Email Subscription', 'orlinski' ),
		'labels'                => $labels,
		'supports'              => array( 'title' ),
		'hierarchical'          => false,
		'public'                => false,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 70,
		'menu_icon'             => 'dashicons-email',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => false,
		'has_archive'           => false,
		'exclude_from_search'   => true,
		'publicly_queryable'    => true,
		'rewrite'               => false,
		'capability_type'       => 'page',
		'show_in_rest'          => false,
	);
	register_post_type( 'email_subscriptions', $args );

}
add_action( 'init', 'subscriptions', 0 );