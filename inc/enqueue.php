<?php
/**
 * Understrap enqueue scripts
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

if ( ! function_exists( 'understrap_scripts' ) ) {
	/**
	 * Load theme's JavaScript and CSS sources.
	 */
	function understrap_scripts() {
		// Get the theme data.
		$the_theme     = wp_get_theme();
		$theme_version = $the_theme->get( 'Version' );

		$css_version = $theme_version . '.' . filemtime( get_template_directory() . '/css/theme.min.css' );
		wp_enqueue_style( 'understrap-styles', get_template_directory_uri() . '/css/theme.min.css', array(), $css_version );

		wp_enqueue_script( 'jquery' );
		wp_enqueue_script( 'understrap-scripts','https://cdnjs.cloudflare.com/ajax/libs/js-cookie/2.2.1/js.cookie.min.js', array(), $js_version, true );
		$js_version = $theme_version . '.' . filemtime( get_template_directory() . '/js/theme.min.js' );
		wp_enqueue_script( 'understrap-scripts', get_template_directory_uri() . '/js/theme.min.js', array(), $js_version, true );
		wp_enqueue_script( 'bootstrap-scripts', get_template_directory_uri() . '/js/bootstrap.min.js', array(), $js_version, true );
		wp_enqueue_script( 'countdown-scripts', get_template_directory_uri() . '/js/countdown.js', array(), $js_version, true );
		if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
			wp_enqueue_script( 'comment-reply' );
		}
	}
} // endif function_exists( 'understrap_scripts' ).

add_action( 'wp_enqueue_scripts', 'understrap_scripts' );

if ( ! function_exists( 'custom_scripts' ) ) {
	/**
	 * Load theme's JavaScript and CSS sources.
	 */
	function custom_scripts() {
		// Get the theme data.
		wp_enqueue_style( 'main-css', get_template_directory_uri() . '/css/main.css', array(), '1.01' );
		wp_enqueue_script( 'main-js', get_template_directory_uri() . '/js/main.js', array(), '1.01', true );
	}
} // endif function_exists( 'custom_scripts' ).

add_action( 'wp_enqueue_scripts', 'custom_scripts' );
