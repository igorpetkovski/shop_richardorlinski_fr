<?php 
//  new  order status
function register_tobecompleted_order_status() {
    register_post_status( 'wc-tobecompleted', array(
        'label'                     => 'To Be Completed',
        'public'                    => true,
        'exclude_from_search'       => false,
        'show_in_admin_all_list'    => true,
        'show_in_admin_status_list' => true,
        'label_count'               => _n_noop( 'To Be Completed (%s)', 'To Be Completed (%s)' )
    ) );
}
add_action( 'init', 'register_tobecompleted_order_status' );


// Add to list of WC Order statuses
function add_tobecompleted_to_order_statuses( $order_statuses ) {
 
    $new_order_statuses = array();
 
    // add new order status after processing
    foreach ( $order_statuses as $key => $status ) {
 
        $new_order_statuses[ $key ] = $status;
 
        if ( 'wc-processing' === $key ) {
            $new_order_statuses['wc-tobecompleted'] = 'To Be Completed';
        }
    }
 
    return $new_order_statuses;
}
add_filter( 'wc_order_statuses', 'add_tobecompleted_to_order_statuses' );



/*
 * Add your custom bulk action in dropdown
 * @since 3.5.0
 */
add_filter( 'bulk_actions-edit-shop_order', 'misha_register_bulk_action' ); // edit-shop_order is the screen ID of the orders page
 
function misha_register_bulk_action1( $bulk_actions ) {
 
    $bulk_actions['mark_wc_tobecompleted'] = 'Mark as To Be Completed'; // <option value="mark_wc_tobecompleted">Mark awaiting shipment</option>
    return $bulk_actions;
 
}
 
/*
 * Bulk action handler
 * Make sure that "action name" in the hook is the same like the option value from the above function
 */
add_action( 'admin_action_mark_wc_tobecompleted', 'misha_bulk_process_custom_status' ); // admin_action_{action name}
 
function misha_bulk_process_custom_status1() {
 
    // if an array with order IDs is not pretobecompleteded, exit the function
    if( !isset( $_REQUEST['post'] ) && !is_array( $_REQUEST['post'] ) )
        return;
 
    foreach( $_REQUEST['post'] as $order_id ) {
 
        $order = new WC_Order( $order_id );
        $order_note = 'Промена на статус:';
        $order->update_status( 'tobecompleted', $order_note, true ); // "misha-shipment" is the order status name (do not use wc-misha-shipment)
 
    }
 
    // of course using add_query_arg() is not required, you can build your URL inline
    $location = add_query_arg( array(
        'post_type' => 'shop_order',
        'marked_wc_tobecompleted' => 1, // markED_awaiting_shipment=1 is just the $_GET variable for notices
        'changed' => count( $_REQUEST['post'] ), // number of changed orders
        'ids' => join( $_REQUEST['post'], ',' ),
        'post_status' => 'all'
    ), 'edit.php' );
 
    wp_redirect( admin_url( $location ) );
    exit;
 
}
 
/*
 * Notices
 */
add_action('admin_notices', 'misha_custom_order_status_notices');
 
function misha_custom_order_status_notices1() {
 
    global $pagenow, $typenow;
 
    if( $typenow == 'shop_order' 
     && $pagenow == 'edit.php'
     && isset( $_REQUEST['marked_wc_tobecompleted'] )
     && $_REQUEST['marked_wc_tobecompleted'] == 1
     && isset( $_REQUEST['changed'] ) ) {
 
        $message = sprintf( _n( 'Order status changed.', '%s order statuses changed.', $_REQUEST['changed'] ), number_format_i18n( $_REQUEST['changed'] ) );
        echo "<div class=\"updated\"><p>{$message}</p></div>";
 
    }
}


