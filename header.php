<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

$container = get_theme_mod( 'understrap_container_type' );
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-MKTTDX4');</script>
<!-- End Google Tag Manager -->

<!-- Ajax protectiong QUEUE -->
<script type="text/javascript" src="//static.queue-it.net/script/queueclient.min.js"></script>
<script
 data-queueit-intercept-domain="shop.richardorlinski.fr"
   data-queueit-intercept="true"
  data-queueit-c="netinside"
  type="text/javascript"
  src="//static.queue-it.net/script/queueconfigloader.min.js">
</script>


	<?php wp_head(); 

	?>
</head>

<body <?php body_class(); ?>>
	<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MKTTDX4"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<?php do_action( 'wp_body_open' ); 

?>
<?php 
if(!isset($_COOKIE['affiliate'])){
    setcookie("affiliate",$_GET['affiliate'],0,"/");
}
else{
	if(isset($_GET['affiliate'])){
	 setcookie("affiliate",$_GET['affiliate'],0,"/");
	}
}
?>

<div class="site" id="page">
<!-- 	<div id="sold-wrapper">
			<p class="my-0 text-uppercase sold-text"><?php 
			if(ICL_LANGUAGE_CODE=='en'):
				_e('People connected','orlinski');
				else:
					_e('Personnes connectées','orlinski');
					endif;?></p>
		<div id="sold-anim" class="sold"></div>
		<div id="sold" class="sold"></div>
	</div>	 -->

	<?php 
	// echo get_template_part('countdown');
	?>
	<div id="wrapper-navbar" itemscope itemtype="http://schema.org/WebSite">
		<!-- marquee -->
		<div class="marquee">
		  <div>
		  </div>
		</div>

		<nav class="navbar  navbar-light border-bottom px-0">

			<div class="container d-flex flex-md-row justify-content-md-between align-items-md-center flex-column justify-content-center align-items-center flex-sm-column justify-content-sm-center align-items-sm-center">

			<!-- Your site title as branding in the menu -->
				<?php if ( is_front_page() && is_home() ) : ?>
				<?php else : ?>
					<a class="navbar-brand order-lg-1 order-md-1" rel="home" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" itemprop="url"><?php bloginfo( 'name' ); ?></a>
				<?php endif; ?>

					<?php
				wp_nav_menu(
					array(
						'theme_location'  => 'primary',
						'container_class' => 'collapse navbar-collapse p-3 order-lg-2 order-3',
						'container_id'    => 'navbarNavDropdown',
						'menu_class'      => 'navbar-nav mx-auto',
						'fallback_cb'     => '',
						'menu_id'         => 'main-menu',
						'depth'           => 2,
						'walker'          => new Understrap_WP_Bootstrap_Navwalker(),
					)
				);
				?>

				<div class="topbar-right-menu d-flex header-menu order-lg-3 order-2">
					<a class="header-icon icon-menu d-lg-none" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="<?php esc_attr_e( 'Toggle navigation', 'understrap' ); ?>">
					<a class="header-icon icon-my-account lrm-login" href="<?php echo get_site_url();?>/my-account/<?php echo '?lang='.ICL_LANGUAGE_CODE;?>">My account</a>
					<a class="header-icon icon-cart" href="#">Cart <span class="item-count xoo-wsc-items-count">
					</span></a>
						<a class="header-icon icon-wishlist" href="<?php echo get_site_url();?>/wishlist/<?php echo '?lang='.ICL_LANGUAGE_CODE;?>">Wishlist</a>
					<a href="<?php echo get_site_url();?>/search" class="header-icon icon-search">Search</a>
					<?php dynamic_sidebar( 'langswitcher' ); ?>
					</a>
				</div>

			</div><!-- .container -->




		</nav><!-- .site-navigation -->

	</div><!-- #wrapper-navbar end -->
		<!-- Submenu  content-->
	<div class="row mx-0 w-100 submenu-content-wrapper position-absolute bg-white">
		<div class="search-form-big col-md-12">
			<div class="container">
				<?php get_template_part('searchform-big');?>
			</div>
		</div>
	</div>

	<!-- submenu content end -->