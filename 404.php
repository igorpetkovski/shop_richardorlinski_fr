<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();

?>
<div class="container">
<section class="no-results not-found my-5 py-5">

	<div class="page-content">
		<div class="row mx-0 align-items-center col-md-10 mx-auto">
		<div class="col-md-8">
		<h1 class="page-title"><?php esc_html_e( 'Nothing Found', 'understrap' ); ?></h1>

		<?php
		if ( is_home() && current_user_can( 'publish_posts' ) ) : ?>

			<p><?php printf( wp_kses( __( 'Ready to publish your first post? <a href="%1$s">Get started here</a>.', 'understrap' ), array(
	'a' => array(
		'href' => array(),
	),
) ), esc_url( admin_url( 'post-new.php' ) ) ); ?></p>

		<?php elseif ( is_search() ) : ?>

			<p><?php esc_html_e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'understrap' ); ?></p>
			<?php
				get_search_form();
		else : ?>

			<p><?php esc_html_e( 'It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', 'understrap' ); ?></p>
			<?php
				get_search_form();
		endif; ?>
	</div>

	<div class="col-md-4">
		<img src="<?php echo get_template_directory_uri();?>/img/kong-baril-404.png" class="img-fluid">
	</div>
	</div>
	</div><!-- .page-content -->

</section><!-- .no-results -->
</div>

<?php get_footer(); ?>
